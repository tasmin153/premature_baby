<html>

<style>
table.blueTable {
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 3px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 15px;
  font-weight: bold;
  color: #240045;
}
table.blueTable thead {
  background: #003C61;
  background: -moz-linear-gradient(top, #406d88 0%, #194f70 66%, #003C61 100%);
  background: -webkit-linear-gradient(top, #406d88 0%, #194f70 66%, #003C61 100%);
  background: linear-gradient(to bottom, #406d88 0%, #194f70 66%, #003C61 100%);
  border-bottom: 0px solid #444444;
}
table.blueTable thead th {
  font-size: 24px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 0px solid #D0E4F5;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot td {
  font-size: 14px;
}
</style>

<title>Premature
</title>

<table class="blueTable">
<thead>
<tr>
<th>head1</th>
<th>head2</th>
</tr>
</thead>
<tbody>
<tr>
<td>cell1_1</td>
<td>cell2_1</td>
</tr>
<tr>
<td>cell1_2</td>
<td>cell2_2</td>
</tr>
<tr>
<td>cell1_3</td>
<td>cell2_3</td>
</tr>
<tr>
<td>cell1_4</td>
<td>cell2_4</td>
</tr>
<tr>
<td>cell1_5</td>
<td>cell2_5</td>
</tr>
<tr>
<td>cell1_6</td>
<td>cell2_6</td>
</tr>
<tr>
<td>cell1_7</td>
<td>cell2_7</td>
</tr>
</tbody>
</table>

</html>