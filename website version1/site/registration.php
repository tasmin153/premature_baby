<?php include('connection.php') ?>
<!DOCTYPE html>

<html lang="en" class="csstransforms csstransforms3d csstransitions"><head></head><body class="homepage" style=""><mfstarttaginclude-header contenteditable="false"></mfstarttaginclude-header>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BabyHelp</title>

    <!-- Bootstrap Core CSS -->
    
    <link rel="stylesheet" href="../common/libs/fonts_awesome/font-awesome/font-awesome/css/font-awesome.min.css">
    <link href="../common/libs/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	<link href="../common/libs/css/style_login.css" rel="stylesheet">
    <link href="../common/libs/css/animate.min.css" rel="stylesheet">
    <link href="../common/libs/css/prettyPhoto.css" rel="stylesheet">
    <link href="../common/libs/css/responsive.css" rel="stylesheet">
    <link href="../common/libs/css/font-awesome.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../common/assets/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../common/assets/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../common/assets/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../common/assets/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../common/assets/apple-touch-icon-57-precomposed.png">
	  <!-- Fonts -->
   
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- IE8 support for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	<header id="header">
        

        <nav class="navbar navbar-inverse" role="banner">
		
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="../common/assets/logo.png"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about-us.php">About Us</a></li>
                        <li><a href="services.php">Services</a></li>
                       
                        <li><a href="contact-us.php">Contact</a></li>                        
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
	
	
	<section id="Registration system">
			<div class="container">
					<div class="center wow fadeInDown">
						<h2>Registration</h2>
						<p class="lead">In the biggest virtual family of babies <br> Join us Today!</p>
					</div>
					
					
					
			<form method="post" action="registration.php">
			<?php include('errors.php'); ?>
				<div class="input-group">
					<label>Firstname</label>
					<input type="text" name="First_Name" value="<?php echo $First_Name; ?>">
				</div>
				<div class="input-group">
					<label>Lastname</label>
					<input type="text" name="Last_Name" value="<?php echo $Last_Name; ?>">
				</div>
				<div class="input-group">
					<label>ContactNo</label>
					<input type="text" name="Contact_No" value="<?php echo $Contact_No; ?>">
				</div>
				<div class="input-group">
					<label>Email</label>
					<input type="email" name="Email" value="<?php echo $Email; ?>">
				</div>
				
				<div class="input-group">
					<label>Gender</label>
					<input type="text" name="Gender" value="<?php echo $Gender; ?>">
				</div>
				<div class="input-group">
					<label>no_of_child</label>
					<input type="text" name="no_of_child" value="<?php echo $no_of_child; ?>">
				</div>
				<div class="input-group">
					<label>no_of_pm_child</label>
					<input type="text" name="no_of_pm_child" value="<?php echo $no_of_child; ?>">
				</div>
				<div class="input-group">
					<label>Password</label>
					<input type="text" name="Password" value="<?php echo $Password; ?>">
				</div>
				<div class="input-group">
					<label>Confirm password</label>
					<input type="password" name="cpassword">
				</div>
				<div class="input-group">
					<button type="submit" class="btn" name="reg_user">Register</button>
				</div>
			<p>Already a member? <a href="login.php">Sign in</a></p>
	</form>
			</div>
	</section>
	

			
	
  
 <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: hidden; animation-duration: 1000ms; animation-delay: 600ms; animation-name: none;">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Copyright</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Support</h3>
                        <ul>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Forum</a></li>
                            <li><a href="#">Documentation</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Developers</h3>
                        <ul>
                            <li><a href="#">Android</a></li>
                            
                            <li><a href="#">Windows</a></li>
                            
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Our Partners</h3>
                        <ul>
							 <li><a href="http://dhakashishuhospital.org.bd/">Dhaka Shishu Hospital</a></li>
							 <li><a href="http://icmh.org.bd/">Institute of Child and mother care health</a></li>
							
							 <li><a href="https://www.unicef.org/bangladesh/media_8202.htm">Unicef Bangladesh</a></li>
							
                          
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    © 2018 <a target="_blank" href="http://shapebootstrap.net/" title="Team Juvenile in action"></a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </footer><!--/#footer-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="../common/libs/js/jquery.js"></script>
    <script src="../common/libs/js/bootstrap.min.js"></script>
    <script src="../common/libs/js/jquery.prettyPhoto.js"></script>
    <script src="../common/libs/js/jquery.isotope.min.js"></script>
    <script src="../common/libs/js/main.js"></script>
    <script src="../common/libs/js/wow.min.js"></script>
    
    <mfendtaginclude-footer contenteditable="false"></mfendtaginclude-footer>


</body>
</html>