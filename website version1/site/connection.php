<?php
session_start();

// initializing variables
$First_Name = "";
$Last_Name = "";
$Contact_No = "";
$Email = "";
$Gender = "";
$no_of_child = "";
$no_of_pm_child = "";
$Password = "";
$cpassword = "";
$errors = array(); 

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'premature baby');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $First_Name = mysqli_real_escape_string($db, $_POST['First_Name']);
  $Last_Name =  mysqli_real_escape_string($db, $_POST['Last_Name']);
  $Contact_No =  mysqli_real_escape_string($db, $_POST['Contact_No']);
  $Email =  mysqli_real_escape_string($db, $_POST['Email']);
  $Gender =  mysqli_real_escape_string($db, $_POST['Gender']);
  $no_of_child =  mysqli_real_escape_string($db, $_POST['no_of_child']);
  $no_of_pm_child = mysqli_real_escape_string($db, $_POST['no_of_pm_child']);
  $Password =  mysqli_real_escape_string($db, $_POST['Password']);
  $cpassword =  mysqli_real_escape_string($db, $_POST['cpassword']);
  

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($First_Name)) { array_push($errors, "First_Name is required"); }
  if (empty($Last_Name)) { array_push($errors, "Last_Name is required"); }
  if (empty($Contact_No)) { array_push($errors, "Contact_No is required"); }
  if (empty($Email)) { array_push($errors, "Email is required"); }
  if (empty($Gender)) { array_push($errors, "Gender is required"); }
  if (empty($no_of_child)) { array_push($errors, "no_of_child is required"); }
  if (empty($no_of_pm_child)) { array_push($errors, "no_of_pm_child is required"); }
  if (empty($Password)) { array_push($errors, "Password is required"); }
  if (empty($cpassword)) { array_push($errors, "cpassword is required"); }
  if ($Password != $cpassword) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM parent WHERE First_Name='$First_Name' OR Email='$Email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['First_Name'] === $First_Name) {
      array_push($errors, "Username already exists");
    }

    if ($user['Email'] === $Email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	//$Password = md5($Password);//encrypt the password before saving in the database

  	//$query = "INSERT INTO users (username, email, password) 
  			  //VALUES('$username', '$email', '$password')";
			  
	$query = "INSERT INTO parent (First_Name,Last_Name,Contact_No,Email,Gender,no_of_child,no_of_pm_child,Password,cpassword)
                     VALUES ('$First_Name','$Last_Name','$Contact_No','$Email','$Gender','$no_of_child',
					 '$no_of_pm_child','$Password','$cpassword')";
  	mysqli_query($db, $query);
  	$_SESSION['First_Name'] = $First_Name;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: index.php');
  }
}

// ... 

// LOGIN USER
if (isset($_POST['login_user'])) {
	$First_Name = mysqli_real_escape_string($db, $_POST['First_Name']);
	$Password =  mysqli_real_escape_string($db, $_POST['Password']);

  if (empty($First_Name)) {
  	array_push($errors, "First_Name is required");
  }
  if (empty($Password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	//$password = md5($password);
  	$query = "SELECT * FROM parent WHERE First_Name='$First_Name' AND Password='$Password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $_SESSION['First_Name'] = $First_Name;
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: index.php');
  	}else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}


// LOGIN USING GMAIL
if (isset($_POST['Gmail_Login'])) {
	$Email = mysqli_real_escape_string($db, $_POST['Email']);


  if (count($errors) == 0) {
  	//$password = md5($password);
  	$query = "SELECT * FROM parent WHERE Email='$Email' ";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $_SESSION['Email'] = $Email;
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: index.php');
  	}else {
  		array_push($errors, "Wrong Email/User doesn't exist ");
  	}
  }
}

?>

































