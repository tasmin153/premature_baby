<?php include('connection.php') ?>
<!DOCTYPE html>

<html lang="en" class="csstransforms csstransforms3d csstransitions"><head></head><body class="homepage" style=""><mfstarttaginclude-header contenteditable="false"></mfstarttaginclude-header>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
	
	
    <meta name="google-signin-client_id" content="331774766859-1s477b3qku8krnq8ue36c5mo120vnct9.apps.googleusercontent.com">


    <title>BabyHelp</title>

    <!-- Bootstrap Core CSS -->
   
    <link rel="stylesheet" href="../common/libs/fonts_awesome/font-awesome/font-awesome/css/font-awesome.min.css">
    <link href="../common/libs/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	<link href="../common/libs/css/style_login.css" rel="stylesheet">
    <link href="../common/libs/css/animate.min.css" rel="stylesheet">
    <link href="../common/libs/css/prettyPhoto.css" rel="stylesheet">
    <link href="../common/libs/css/responsive.css" rel="stylesheet">
    <link href="../common/libs/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../common/assets/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../common/assets/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../common/assets/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../common/assets/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../common/assets/apple-touch-icon-57-precomposed.png">
	  <!-- Fonts -->
   
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- IE8 support for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	<header id="header">
        

        <nav class="navbar navbar-inverse" role="banner">
		
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="../common/assets/logo.png"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about-us.php">About Us</a></li>
                        <li><a href="services.php">Services</a></li>
                       
                        <li><a href="contact-us.php">Contact</a></li>                        
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
	

	<section id="Login System">
			<div class="container">
					<div class="center wow fadeInDown">
						<h2>Login</h2>
						<p class="lead">Sign in and discover what you want to know </p>
					</div>
	 
					  <form method="post" action="login.php">
					<?php include('errors.php'); ?>
						<div class="input-group">
							<label>Username</label>
							<input type="text" name="First_Name" >
						</div>
						<div class="input-group">
							<label>Password</label>
							<input type="password" name="Password">
						</div>
						<div class="input-group">
						<button type="submit" class="btn" name="login_user">Login</button>
						</div>
						
						
						
						<div class = "center wow fadeInDown">
								    <style>
									.g-signin2{
									margin-left: 100px;
									margin-top: 30px;
							}
							.data{
							display: none;
								}
								</style>
								<body>
						
										<div class="g-signin2" data-onsuccess="onSignIn"></div>
										<div class="data">
												<p>profile details</p>
												<img id="pic" class="img-circle" width="100" height="100"/>
												
												<p>Email address</p>
												<p id="Email" class="alert alert-primary"></p>
							
						
										<div class="input-group">
										<button type="submit" class="btn" name="Gmail_login">Sign in</button>
										</div>
										<button onclick="SignOut()" class="btn btn-danger">Sign Out</button>
										</div>
										</form>
								</body>
	
							<br>Not yet a member? <a href="registration.php">Sign up</a></br>
				
						</div>
					
			</div>
			
		</section>
		
	
  
<section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: hidden; animation-duration: 1000ms; animation-delay: 600ms; animation-name: none;">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Copyright</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Support</h3>
                        <ul>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Forum</a></li>
                            <li><a href="#">Documentation</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Developers</h3>
                        <ul>
                            <li><a href="#">Android</a></li>
                            
                            <li><a href="#">Windows</a></li>
                            
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Our Partners</h3>
                        <ul>
							 <li><a href="http://dhakashishuhospital.org.bd/">Dhaka Shishu Hospital</a></li>
							 <li><a href="http://icmh.org.bd/">Institute of Child and mother care health</a></li>
							
							 <li><a href="https://www.unicef.org/bangladesh/media_8202.htm">Unicef Bangladesh</a></li>
							
                          
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    © 2018 <a target="_blank" href="http://shapebootstrap.net/" title="Team Juvenile in action"></a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </footer><!--/#footer-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="../common/libs/js/jquery.js"></script>
    <script src="../common/libs/js/bootstrap.min.js"></script>
    <script src="../common/libs/js/jquery.prettyPhoto.js"></script>
    <script src="../common/libs/js/jquery.isotope.min.js"></script>
    <script src="../common/libs/js/main.js"></script>
    <script src="../common/libs/js/wow.min.js"></script>
	<script src="../common/libs/js/script_gmail.js"></script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	
    
    <mfendtaginclude-footer contenteditable="false"></mfendtaginclude-footer>


</body>
</html>
