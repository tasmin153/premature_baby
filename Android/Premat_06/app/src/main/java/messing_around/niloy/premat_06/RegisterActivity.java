package messing_around.niloy.premat_06;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class RegisterActivity extends AppCompatActivity {

    EditText edit_user;
    EditText edit_email;
    EditText edit_pass;
    Button btn_reg;

    private static final String REGISTER_URL="http://niloybiswas.000webhostapp.com/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        edit_user = (EditText)findViewById(R.id.reg_user);
        edit_email = (EditText)findViewById(R.id.reg_email);
        edit_pass = (EditText)findViewById(R.id.reg_pass);
        btn_reg = (Button)findViewById(R.id.reg_btn);
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

    }

    private void registerUser() {
        String user = edit_user.getText().toString().trim().toLowerCase();
        String email = edit_email.getText().toString().trim().toLowerCase();
        String password = edit_pass.getText().toString().trim().toLowerCase();

        register(user,email,password);
    }

    private void register(String user, String password, String email){
        String urlSuffix="?username="+user+"&password="+password+"&email="+email;
        class RegisterUser extends AsyncTask<String,String,String>{
           ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading=ProgressDialog.show(RegisterActivity.this,"Please Wait",null,true,true);
            }

            protected void onPostExecute(){
                super.onPreExecute();
                Toast.makeText(getApplicationContext(),"Registered", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... params) {
               String s = params[0];
                BufferedReader bufferedReader = null;
                try{
                    URL url = new URL(REGISTER_URL+s);
                    HttpURLConnection con=(HttpURLConnection)url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String result;
                    result= bufferedReader.readLine();
                    return result;



                }catch (Exception e){
                    return null;
                }
            }
        }
        RegisterUser ur = new RegisterUser();
        ur.execute(urlSuffix);
    }
}
