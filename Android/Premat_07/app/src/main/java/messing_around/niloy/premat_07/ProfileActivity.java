package messing_around.niloy.premat_07;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.LoginFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.speech.RecognizerIntent;

import java.util.ArrayList;
import java.util.Locale;

public class ProfileActivity extends AppCompatActivity {

    private TextView txvResult;
    TextView textView;
//    Button logout;
    ImageView ico_symptoms;
    ImageView ico_maps;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txvResult = (TextView) findViewById(R.id.txvResult);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PrematureBaby");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitle("Team Juvenile");
        toolbar.setSubtitleTextColor(Color.GRAY);



        // Assign ID's to textview and button.
        textView = (TextView)findViewById(R.id.TextViewUserEmail);
    //    logout = (Button)findViewById(R.id.btn_logout);

        // Receiving value into activity using intent.
        String TempHolder = getIntent().getStringExtra("UserEmailTAG");

        // Setting up received value into TextView.
        textView.setText(textView.getText() + TempHolder);

        ico_symptoms = (ImageView)findViewById(R.id.ico_symptomp);
        ico_maps = (ImageView) findViewById(R.id.ico_maps);

       // final View imageButton = findViewById(R.id.ico_symptoms);
        ico_symptoms.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, SymptomsActivity.class);

                startActivity(intent);
            }
        });

    /*    // Adding click listener to logout button.
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Showing Echo Response Message Coming From Server.
                Toast.makeText(ProfileActivity.this, "Logged Out Successfully", Toast.LENGTH_LONG).show();

                // Closing the current activity.
                finish();

                // Redirect to Main Login activity after log out.
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);

                startActivity(intent);

            }
        });  */





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        String msg = "";

        switch (item.getItemId()){
            case R.id.profile_edit:
                System.out.print("Delete");
                break;
            case R.id.complain_box:
                msg="Contact us";
                break;
            case R.id.logout_option:
                // Showing Echo Response Message Coming From Server.
                Toast.makeText(ProfileActivity.this, "Logged Out Successfully", Toast.LENGTH_LONG).show();

                // Closing the current activity.
                finish();

                // Redirect to Main Login activity after log out.
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);

                startActivity(intent);

                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txvResult.setText(result.get(0));
                }
                break;
        }
    }



}
